//
//  DataStorage.swift
//  notes-ios
//
//  Created by Алексей Агильдин on 17.03.2021.
//

import Foundation

class DataStorage {
    
    static let shared = DataStorage()
    
    private init() {}
    
    private let userDefaults = UserDefaults.standard
    private let encoder = JSONEncoder()
    private let decoder = JSONDecoder()
    
    // saved notes in memory
    var notes: [Note] {
        get {
            guard let savedData = userDefaults.object(forKey: #function) as? Data else { return [] }
            do {
                let array = try decoder.decode([Note].self, from: savedData)
                return array
            } catch {
                return []
            }
        }
        set {
            let encodedData = try? encoder.encode(newValue)
            userDefaults.set(encodedData, forKey: #function)
        }
    }
    
    // get info about first launch
    var isFirstLaunch: Bool {
        get {
            return !userDefaults.bool(forKey: #function)
        }
        
        set {
            userDefaults.set(!newValue, forKey: #function)
        }
    }
}


