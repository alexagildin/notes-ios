//
//  Note.swift
//  notes-ios
//
//  Created by Алексей Агильдин on 17.03.2021.
//

import Foundation
import UIKit

enum FontType: Int {
    case standart, bold, italic, underline
}

class Note: Codable {
    
    var id: String
    var text: String {
        didSet {
            if text == "" {
                fontSizes = []
                fontTypes = []
                ranges = []
            }
        }
    }
    var fontSizes: [CGFloat]
    var fontTypes: [FontType]
    var ranges: [NSRange]
    var timestamp: Double?
    
    init(id: String = "", text: String = "", fontSizes: [CGFloat] = [], fontTypes: [FontType] = [], ranges: [NSRange] = [], timestamp: Double? = nil) {
        self.id = id == "" ? String.randomString(length: 10) : id
        self.text = text
        self.fontSizes = fontSizes
        self.fontTypes = fontTypes
        self.ranges = ranges
        self.timestamp = timestamp
    }
    
    enum CodingKeys: String, CodingKey {
        case id, text, fontSizes, fontTypes, ranges, timestamp
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(text, forKey: .text)
        try container.encode(fontSizes, forKey: .fontSizes)
        try container.encode(fontTypes.map({ return $0.rawValue }), forKey: .fontTypes)
        try container.encode(ranges, forKey: .ranges)
        try container.encode(timestamp, forKey: .timestamp)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        text = try container.decode(String.self, forKey: .text)
        fontSizes = try container.decode([CGFloat].self, forKey: .fontSizes)
        fontTypes = try container.decode([Int].self, forKey: .fontTypes).map({ return FontType(rawValue: $0)! })
        ranges = try container.decode([NSRange].self, forKey: .ranges)
        do {
            timestamp = try container.decode(Double.self, forKey: .timestamp)
        } catch {
            timestamp = nil
        }
    }
    
}
extension Note {
    var title: String {
        if let index = text.firstIndex(of: "\n") {
            return String(text[text.startIndex...index])
        } else {
            return text
        }
    }
    
    var attributedString: NSMutableAttributedString {
        let attrString = NSMutableAttributedString(string: text)
        attrString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 16), range: NSRange(location: 0, length: text.count))
        for i in 0..<ranges.count {
            switch fontTypes[i] {
            case .bold:
                attrString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: fontSizes[i]), range: ranges[i])
                break
            case .italic:
                attrString.addAttribute(NSAttributedString.Key.font, value: UIFont.italicSystemFont(ofSize: fontSizes[i]), range: ranges[i])
                break
            case .underline:
                attrString.addAttribute(kCTUnderlineStyleAttributeName as NSAttributedString.Key, value: NSUnderlineStyle.single.rawValue, range: ranges[i])
                break
            default: break
            }
        }
        attrString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.textColor, range: NSRange(location: 0, length: text.count))
        return attrString
    }
    
    func addAttr(fontSize: CGFloat, fontType: FontType, range: NSRange) {
        
        
    }
    
    
}
