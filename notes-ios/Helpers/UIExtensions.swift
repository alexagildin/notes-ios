//
//  UIExtensions.swift
//  notes-ios
//
//  Created by Алексей Агильдин on 17.03.2021.
//

import UIKit

extension UIViewController {

    // method for create viewcontroller based on name viewcontroller/storyboard
    func createViewControllerFromStoryboard<T: UIViewController>(storyboardName: T.Type) -> T {
        return UIViewController.createViewControllerFromStoryboard(storyboardName: storyboardName)
    }
    
    // static method for create viewcontroller based on name viewcontroller/storyboard
    static func createViewControllerFromStoryboard<T: UIViewController>(storyboardName: T.Type) -> T {
        let name = NSStringFromClass(storyboardName).components(separatedBy: ".").last!
        let storyboard = UIStoryboard(name: name, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: name) as! T
        return controller
    }
    
    // method for identify that current mode is dark
    var isDarkMode: Bool {
        if #available(iOS 13.0, *) {
            return self.traitCollection.userInterfaceStyle == .dark
        } else {
            return false
        }
    }
}

extension UIView {
    
    // method for round corners with radius
    func roundCorners(radius: CGFloat) {
        self.clipsToBounds = true
        self.layer.cornerRadius = radius
    }
    
    // method for show object
    @objc
    func showObject(_ duration: Double = 0.5){
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1
            self.isHidden = false
        }) { (_) in
            
        }
    }
    
    // method for hide object
    @objc
    func hideObject(_ duration: Double = 0.5){
        if duration > 0 {
            UIView.animate(withDuration: duration, animations: {
                self.alpha = 0
            }) { (_) in
                self.isHidden = true
            }
        } else {
            self.alpha = 0
            self.isHidden = true
        }
    }
}

extension UIColor {
    // random color
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
    
    // init method for get color based on rgb values
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    // init method for get color based on 0x000000
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    // color for background of each viewcontroller
    static var backgroundColor: UIColor {
        if UIViewController().isDarkMode {
            return .init(rgb: 0x000A1F)
        } else {
            return .init(rgb: 0xB0C5F5)
        }
    }
    
    // color for main element at viewcontroller
    static var mainBackColor: UIColor {
        if UIViewController().isDarkMode {
            return .init(rgb: 0x091224)
        } else {
            return .init(rgb: 0xC2D1F5)
        }
    }
    
    // color for background top bar
    static var topBarColor: UIColor {
        if UIViewController().isDarkMode {
            return .init(rgb: 0x000F30)
        } else {
            return .init(rgb: 0xB0C5F5)
        }
    }
    
    // color for title, buttons at top bar
    static var topBarTextColor: UIColor {
        if UIViewController().isDarkMode {
            return .init(rgb: 0x466099)
        } else {
            return .init(rgb: 0x3A59A0)
        }
    }
    
    // color for text
    static var textColor: UIColor {
        if UIViewController().isDarkMode {
            return .white
        } else {
            return .black
        }
    }
}

extension UITextView {
    // method for get estimated height of textView
    @objc
    func calculateHeight() -> CGFloat {
        let textView = UITextView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: CGFloat.greatestFiniteMagnitude))
        textView.font = self.font
        textView.text = text
        textView.textContainerInset = textContainerInset
        textView.textContainer.lineFragmentPadding = textContainer.lineFragmentPadding
        textView.sizeToFit()
        return textView.frame.size.height
    }
}

extension NSNotification.Name {
    // notification for change mode state of something
    static let modeChanged = NSNotification.Name("modeChanged")
    // notification for save note from list after editing
    static let saveNote = NSNotification.Name("saveNote")
}

extension CGFloat {
    // return bottom offset of screen
    static var bottomOffset: CGFloat {
        var offset: CGFloat = 0
        if #available(iOS 11.0, *), UIApplication.shared.windows.count > 0  {
            let window = UIApplication.shared.windows[0]
            offset = window.safeAreaInsets.bottom
        }
        return offset
    }
    
    // return top offset of screen
    static var topOffset: CGFloat {
        var offset: CGFloat = 20
        if #available(iOS 11.0, *), UIApplication.shared.windows.count > 0 {
            let window = UIApplication.shared.windows[0]
            if window.safeAreaInsets.top > 0 {
                offset = window.safeAreaInsets.top
            }
        }
        return offset
    }
}

public protocol NibLoadable {
    static var nibName: String { get }
}

extension NibLoadable where Self: UIView {

    static var nibName: String {
        return String(describing: Self.self)
    }

    static var nib: UINib {
        let bundle = Bundle(for: Self.self)
        return UINib(nibName: Self.nibName, bundle: bundle)
    }

    func setupFromNib() {
        guard let view = Self.nib.instantiate(withOwner: self, options: nil).first as? UIView else { fatalError("Error loading \(self) from nib") }
        addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
    }
}
