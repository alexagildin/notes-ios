//
//  FoundationExtensions.swift
//  notes-ios
//
//  Created by Алексей Агильдин on 18.03.2021.
//

import Foundation

extension NSObject {
    
    // method for get className
    class var className: String {
        return String(describing: self)
    }
}
extension String {
    // method for get random string with length
    static func randomString(length: Int) -> String {
      let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
      return String((0..<length).map{ _ in letters.randomElement()! })
    }
}
