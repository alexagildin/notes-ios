//
//  AdditionalKeyboardView.swift
//  notes-ios
//
//  Created by Алексей Агильдин on 19.03.2021.
//

import UIKit

protocol AdditionalKeyboardViewDelegate: class {
    func boldChangeState(state: Bool)
    func coursiveChangeState(state: Bool)
    func underlineChangeState(state: Bool)
    func fontSizeChange(more: Bool)
}

class AdditionalKeyboardView: UIView, NibLoadable {

    @IBOutlet weak var boldButton: UIButton! {
        didSet {
            boldButton.setTitle("B", for: .normal)
            boldButton.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .bold)
            boldButton.setTitleColor(.textColor, for: .normal)
            boldButton.addTarget(self, action: #selector(boldButtonTouch), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var coursiveButton: UIButton! {
        didSet {
            coursiveButton.setTitle("I", for: .normal)
            coursiveButton.titleLabel?.font = UIFont.italicSystemFont(ofSize: 16)
            coursiveButton.setTitleColor(.textColor, for: .normal)
            coursiveButton.addTarget(self, action: #selector(coursiveButtonTouch), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var underlineButton: UIButton! {
        didSet {
            let attrString = NSMutableAttributedString(string: "U")
            let range = NSRange(location: 0, length: 1)
            
            attrString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 16), range: range)
            attrString.addAttribute(kCTUnderlineStyleAttributeName as NSAttributedString.Key, value: NSUnderlineStyle.single.rawValue, range: range)
            
            underlineButton.setAttributedTitle(attrString, for: .normal)
            underlineButton.setTitleColor(.textColor, for: .normal)
            underlineButton.addTarget(self, action: #selector(underlineButtonTouch), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var moreFontButton: UIButton! {
        didSet {
            moreFontButton.setTitle("A+", for: .normal)
            moreFontButton.titleLabel?.font = UIFont.systemFont(ofSize: 18)
            moreFontButton.setTitleColor(.textColor, for: .normal)
            moreFontButton.addTarget(self, action: #selector(moreFontButtonTouch), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var lessFontButton: UIButton! {
        didSet {
            lessFontButton.setTitle("A-", for: .normal)
            lessFontButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
            lessFontButton.setTitleColor(.textColor, for: .normal)
            lessFontButton.addTarget(self, action: #selector(lessFontButtonTouch), for: .touchUpInside)
        }
    }
    
    weak var delegate: AdditionalKeyboardViewDelegate?
    
    fileprivate var boldState = false {
        didSet {
            if boldButton != nil {
                if boldState {
                    boldButton.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .bold)
                    coursiveState = false
                } else {
                    boldButton.titleLabel?.font = UIFont.systemFont(ofSize: 16)
                }
                delegate?.boldChangeState(state: boldState)
            }
        }
    }
    fileprivate var coursiveState = false {
        didSet {
            if coursiveButton != nil {
                if coursiveState {
                    coursiveButton.titleLabel?.font = UIFont.italicSystemFont(ofSize: 16)
                    boldState = false
                } else {
                    coursiveButton.titleLabel?.font = UIFont.systemFont(ofSize: 16)
                }
                delegate?.coursiveChangeState(state: coursiveState)
            }
        }
    }
    fileprivate var underlineState = false {
        didSet {
            if underlineButton != nil {
                let attrString = NSMutableAttributedString(string: "U")
                let range = NSRange(location: 0, length: 1)
                if coursiveState {
                    attrString.addAttribute(kCTUnderlineStyleAttributeName as NSAttributedString.Key, value: NSUnderlineStyle.single.rawValue, range: range)
                }
                underlineButton.setAttributedTitle(attrString, for: .normal)
                delegate?.underlineChangeState(state: underlineState)
            }
        }
    }
    fileprivate var fontSize: CGFloat = 16
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupFromNib()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupFromNib()
    }
    
    func checkForMode(){
        backgroundColor = .backgroundColor
        boldButton.setTitleColor(.textColor, for: .normal)
        coursiveButton.setTitleColor(.textColor, for: .normal)
        underlineButton.setTitleColor(.textColor, for: .normal)
        moreFontButton.setTitleColor(.textColor, for: .normal)
        lessFontButton.setTitleColor(.textColor, for: .normal)
    }
    
    @objc
    func boldButtonTouch(){
        boldState = !boldState
    }
    
    @objc
    func coursiveButtonTouch(){
        coursiveState = !coursiveState
    }
    
    @objc
    func underlineButtonTouch(){
        underlineState = !underlineState
    }
    
    @objc
    func moreFontButtonTouch(){
        delegate?.fontSizeChange(more: true)
    }
    
    @objc
    func lessFontButtonTouch(){
        delegate?.fontSizeChange(more: false)
    }
    
    
}
