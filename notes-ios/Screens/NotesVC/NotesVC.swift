//
//  NotesVC.swift
//  notes-ios
//
//  Created by Алексей Агильдин on 17.03.2021.
//

import UIKit

class NotesVC: BaseVC {

    fileprivate var tableView: UITableView!

    fileprivate var selectedIndex: IndexPath?
    
    fileprivate var notes: [Note] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Заметки"
        changeTopBarItems(edit: false)
        
        if DataStorage.shared.isFirstLaunch {
            let firstNoteText = "Первоначальная заметка, появляется только при первом запуске приложения\n\n" +
            "Заметки сохраняются между сеансами\n\n" +
            "Из списка заметок можно выбрать определенную заметку и ее отредактировать в новом окне, для этого требуется нажать на заметку\n\n" +
            "Заметка будет сохраняться автоматически при выходе с экрана редактирования заметки (если заметка пустая, то заметка сохраняться не будет)\n\n" +
            "Также редактирование заметки возможно из общего списка заметок, для этого нужно зажать определенную заметку\n" +
            "Для завершения редактирования и сохранения изменений нажмите \"сохранить\" в правом верхнем углу, для завершения редактирования без сохранения нажмите \"отмена\" в левом верхнем углу\n\n" +
            "Для добавления новой заметки нажмите на \"+\" в правом верхнем углу\n\n" +
            "Для удаления заметки сделайте свайп на заметке, которую хотите удалить и нажмите \"Удалить\"\n\n"
            
            notes = [Note(id: "", text: firstNoteText, fontSizes: [], fontTypes: [], ranges: [], timestamp: nil)]
            DataStorage.shared.isFirstLaunch = false
        } else {
            notes = DataStorage.shared.notes
        }
        
        tableView = UITableView(frame: CGRect(x: 16, y: .topOffset + 44 + 16, width: view.frame.width - 32, height: view.frame.height - .topOffset - 44 - 16 - .bottomOffset))
        tableView.backgroundColor = .mainBackColor
        tableView.register(UINib(nibName: NoteTableViewCell.className, bundle: nil), forCellReuseIdentifier: "noteCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.roundCorners(radius: 10)
        view.addSubview(tableView)
        
        checkForMode()
    }
    
    func changeTopBarItems(edit: Bool){
        if edit {
            let rightBarButtonItem = UIBarButtonItem(title: "Сохранить", style: .done, target: self, action: #selector(sendNotifSaveNote))
            rightBarButtonItem.tintColor = .topBarTextColor
            navigationItem.rightBarButtonItem = rightBarButtonItem
            let leftBarButtonItem = UIBarButtonItem(title: "Отмена", style: .done, target: self, action: #selector(cancelEdit))
            leftBarButtonItem.tintColor = .topBarTextColor
            navigationItem.leftBarButtonItem = leftBarButtonItem
        } else {
            let rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "plus")?.withRenderingMode(.alwaysTemplate), style: .done, target: self, action: #selector(addNote))
            rightBarButtonItem.tintColor = .topBarTextColor
            navigationItem.rightBarButtonItem = rightBarButtonItem
            navigationItem.leftBarButtonItem = nil
        }
    }
    
    // overriding method for change colors if mode is changed (light / dark)
    override func checkForMode() {
        super.checkForMode()
        NotificationCenter.default.post(name: .modeChanged, object: nil)
        tableView.backgroundColor = .mainBackColor
        navigationItem.leftBarButtonItem?.tintColor = .topBarTextColor
        navigationItem.rightBarButtonItem?.tintColor = .topBarTextColor
    }
    
    // method for open viewcontroller for add new note
    @objc
    func addNote(){
        checkForEditing(newValue: nil)
        let vc = createViewControllerFromStoryboard(storyboardName: EditNoteVC.self)
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc
    func sendNotifSaveNote(){
        NotificationCenter.default.post(name: .saveNote, object: nil)
    }
    
    @objc
    func cancelEdit(){
        checkForEditing(newValue: nil)
    }
    
    func checkForEditing(newValue: IndexPath?){
        var savedIndex: IndexPath?
        if let index = selectedIndex {
            savedIndex = index
        } else {
            tableView.becomeFirstResponder()
        }
        selectedIndex = newValue
        if let index = savedIndex {
            changeTopBarItems(edit: false)
            tableView.reloadRows(at: [index], with: .automatic)
        }
        if let index = selectedIndex {
            changeTopBarItems(edit: true)
            tableView.reloadRows(at: [index], with: .automatic)
        }
    }
    
    override func keyboardShown(height: CGFloat) {
        if height > 0 {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: height, right: 0)
        } else {
            tableView.contentInset = .zero
        }
        tableView.scrollIndicatorInsets = tableView.contentInset

//        let selectedRange = tableView.selectedRange
//        tableView.scrollRangeToVisible(selectedRange)
    }
    

}
extension NotesVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath == selectedIndex {
            return 200
        } else {
            return 55
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "noteCell", for: indexPath) as! NoteTableViewCell
        cell.setupCell(with: notes[indexPath.row], delegate: self)
        cell.couldEdit = indexPath == selectedIndex
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard selectedIndex != indexPath else { return }
        checkForEditing(newValue: nil)
        let vc = createViewControllerFromStoryboard(storyboardName: EditNoteVC.self)
        vc.flagNew = false
        vc.data = notes[indexPath.row]
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Удалить") { (action, indexPath) in
            let alertVC = UIAlertController(title: "Внимание", message: "Вы уверены, что хотите удалить заметку?", preferredStyle: .alert)
            alertVC.addAction(UIAlertAction(title: "Отмена", style: .destructive, handler: nil))
            alertVC.addAction(UIAlertAction(title: "Да", style: .default, handler: { (_) in
                self.notes.remove(at: indexPath.row)
                DataStorage.shared.notes = self.notes
                tableView.deleteRows(at: [indexPath], with: .fade)
            }))
            self.present(alertVC, animated: true, completion: nil)
        }
        return [delete]
    }
}
extension NotesVC: EditNoteVCDelegate {
    func editNote(note: Note, new: Bool) {
        guard note.text != "" else { return }
        if new {
            notes = [note] + DataStorage.shared.notes
            tableView.reloadData()
            DataStorage.shared.notes = notes
        } else {
            if let index = notes.firstIndex(where: { $0.id == note.id }) {
                notes[index] = note
                tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
            }
        }
    }
}
extension NotesVC: NoteTableViewCellDelegate {
    func longPressed(data: Note) {
        if let index = notes.firstIndex(where: { $0.id == data.id }) {
            checkForEditing(newValue: IndexPath(row: index, section: 0))
        }
    }
    
    func saveNote(data: Note){
        guard data.text != "" else { return }
        if let index = notes.firstIndex(where: { $0.id == data.id }) {
            notes[index] = data
            DataStorage.shared.notes = notes
            selectedIndex = nil
            tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
        }
        changeTopBarItems(edit: false)
    }
}
