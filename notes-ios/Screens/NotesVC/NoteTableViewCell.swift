//
//  NoteTableViewCell.swift
//  notes-ios
//
//  Created by Алексей Агильдин on 18.03.2021.
//

import UIKit

protocol NoteTableViewCellDelegate: class {
    func longPressed(data: Note)
    func saveNote(data: Note)
}

class NoteTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = .textColor
        }
    }
    @IBOutlet weak var textView: UITextView! {
        didSet {
            textView.backgroundColor = .clear
            textView.textContainerInset = UIEdgeInsets.zero
            textView.textContainer.lineFragmentPadding = 0
            textView.textColor = .textColor
            textView.isEditable = false
            textView.isScrollEnabled = false
            textView.isSelectable = false
            textView.delegate = self
        }
    }
    @IBOutlet weak var controlView: UIView! {
        didSet {
            controlView.isHidden = true
            let longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressed))
            contentView.addGestureRecognizer(longPressGestureRecognizer)
        }
    }
    
    fileprivate var data: Note!
    weak var delegate: NoteTableViewCellDelegate?
    
    var couldEdit: Bool = false {
        didSet {
            textView.isEditable = couldEdit
            textView.isScrollEnabled = couldEdit
            textView.isSelectable = couldEdit
            controlView.isHidden = couldEdit
            if couldEdit {
                textView.becomeFirstResponder()
            } else {
                textView.resignFirstResponder()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: animated)
    }
    
    func setupCell(with itemData: Note, delegate: NoteTableViewCellDelegate) {
        self.data = itemData
        self.delegate = delegate
        
        titleLabel.text = itemData.title
        textView.text = itemData.text
        
        NotificationCenter.default.addObserver(self, selector: #selector(modeChanged), name: .modeChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(saveNote), name: .saveNote, object: nil)
    }
    
    @objc
    func longPressed(){
        delegate?.longPressed(data: data)
    }
    
    @objc
    func modeChanged(){
        titleLabel.textColor = .textColor
        textView.textColor = .textColor
    }
    
    @objc
    func saveNote(){
        guard couldEdit else { return }
        textView.resignFirstResponder()
        delegate?.saveNote(data: data)
    }

}
extension NoteTableViewCell: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        data.text = textView.text
        titleLabel.text = data.title
    }
}
