//
//  EditNoteVC.swift
//  notes-ios
//
//  Created by Алексей Агильдин on 17.03.2021.
//

import UIKit

protocol EditNoteVCDelegate: class {
    func editNote(note: Note, new: Bool)
}

class EditNoteVC: BaseVC {

    fileprivate var textView: UITextView!
    fileprivate var additionalView: AdditionalKeyboardView!
    
    var data: Note = Note()
    var flagNew: Bool = true
    weak var delegate: EditNoteVCDelegate?
    
    fileprivate var fontSize: CGFloat = 16
    fileprivate var fontType: FontType = .standart
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back")?.withRenderingMode(.alwaysTemplate), style: .done, target: self, action: #selector(backTouch))
        leftBarButtonItem.tintColor = .topBarTextColor
        navigationItem.leftBarButtonItem = leftBarButtonItem
        
        textView = UITextView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        textView.backgroundColor = .clear
        textView.textColor = .textColor
        textView.font = UIFont.systemFont(ofSize: 16)
        textView.delegate = self
        
        additionalView = AdditionalKeyboardView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 50))
        additionalView.delegate = self
        
        view.addSubview(textView)
        
        textView.attributedText = data.attributedString
        
        if flagNew {
            title = "Новая заметка"
            textView.becomeFirstResponder()
        } else {
            title = "Редактирование заметки"
        }
        
        checkForMode()
    }
    
    @objc
    func backTouch(){
        navigationController?.popViewController(animated: true)
    }
    
    override func keyboardShown(height: CGFloat) {
        if height > 0 {
            textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: height, right: 0)
        } else {
            textView.contentInset = .zero
        }
        textView.scrollIndicatorInsets = textView.contentInset

        let selectedRange = textView.selectedRange
        textView.scrollRangeToVisible(selectedRange)
    }
    
    func keyboardWillDisappear(notification: Notification) {
        textView.frame.size.height = view.frame.height
    }
    
    override func checkForMode() {
        super.checkForMode()
        textView.textColor = .textColor
        navigationItem.leftBarButtonItem?.tintColor = .topBarTextColor
        additionalView.checkForMode()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        delegate?.editNote(note: data, new: flagNew)
        textView.endEditing(true)
    }
    

}
extension EditNoteVC: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        data.text = textView.text
        textView.attributedText = data.attributedString
    }
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        if textView.selectedRange.length > 0 {
//            textView.inputAccessoryView = additionalView
        } else {
            textView.inputAccessoryView = nil
        }
    }
}
extension EditNoteVC: AdditionalKeyboardViewDelegate {
    
    func boldChangeState(state: Bool) {
        if textView.selectedRange.length > 0 {
//            data.addAttr(fontSize: <#T##CGFloat#>, fontType: <#T##FontType#>, range: <#T##NSRange#>)
        } else {
//            if state {
//                fontType = .bold
//            } else if fontType == .bold {
//                fontType = .standart
//            }
        }
    }
    
    func coursiveChangeState(state: Bool) {
        if textView.selectedRange.length > 0 {
            
        } else {
//            if state {
//                fontType = .italic
//            } else if fontType == .italic {
//                fontType = .standart
//            }
        }
    }
    
    func underlineChangeState(state: Bool) {
        if textView.selectedRange.length > 0 {
            
        } else {
//            if state {
//                fontType = .underline
//            } else if fontType == .underline {
//                fontType = .standart
//            }
        }
    }
    
    func fontSizeChange(more: Bool) {
        if textView.selectedRange.length > 0 {
            
        } else {
//            if more {
//                if fontSize < 30 {
//                    fontSize += 1
//                }
//            } else {
//                if fontSize > 8 {
//                    fontSize -= 1
//                }
//            }
        }
    }
}
