//
//  BaseVC.swift
//  notes-ios
//
//  Created by Алексей Агильдин on 18.03.2021.
//

import UIKit

class BaseVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem?.title = ""
        
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        checkForMode()
    }
    
    // method for change colors if mode is changed (light / dark)
    func checkForMode(){
        view.backgroundColor = .backgroundColor
        navigationController?.navigationBar.barTintColor = .topBarColor
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.topBarTextColor]
        navigationItem.rightBarButtonItem?.tintColor = .topBarTextColor
        navigationItem.leftBarButtonItem?.tintColor = .topBarTextColor
    }
    
    @objc
    func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }

        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)

        
        if notification.name == UIResponder.keyboardWillHideNotification {
            keyboardShown(height: 0)
        } else {
            keyboardShown(height: keyboardViewEndFrame.height - .bottomOffset)
        }
    }
    
    func keyboardShown(height: CGFloat) {
        
    }

}
