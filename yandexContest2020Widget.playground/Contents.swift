
import SwiftUI
import PlaygroundSupport

struct CustomTextField: View {
    var placeholder: Text
    @Binding var text: String
    var editingChanged: (Bool)->() = { _ in }
    var commit: ()->() = { }

    var body: some View {
        ZStack(alignment: .leading) {
            if text.isEmpty { placeholder }
            TextField("", text: $text, onEditingChanged: editingChanged, onCommit: commit)
        }
    }
}

struct ContentView: View {
    @State private var text: String = ""
    var body: some View {
        ZStack {
            Color(.black)
            VStack() {
                HStack {
                    let date = Date()
                    let calendar = Calendar.current
                    let hour = calendar.component(.hour, from: date)
                    let minutes = calendar.component(.minute, from: date)
                    
                    Text("\(hour)")
                        .font(.system(size: 30, weight: .light, design: .default))
                        .frame(width: 40, height: 30, alignment: .center)
                    
                    VStack {
                        Text(".")
                            .font(.system(size: 18, weight: .bold, design: .default))
                            .frame(width: 10, height: 10, alignment: .center)
                        
                        Text(".")
                            .font(.system(size: 18, weight: .bold, design: .default))
                            .frame(width: 10, height: 10, alignment: .center)
                    }
                    
                    
                    Text("\(minutes)").font(.system(size: 30, weight: .light, design: .default))
                        .frame(width: 40, height: 30, alignment: .center)
                }
                .frame(width: 130, height: 50, alignment: .leading)
                
                CustomTextField(
                            placeholder: Text("Y").foregroundColor(.red),
                            text: $text
                        )
                .background(Color.white)
                .cornerRadius(20)
                .frame(width: 130, height: 40, alignment: .leading)
                .font(.system(size: 24, weight: .light, design: .default))
                    .foregroundColor(.black)
            }
            .frame(width: 150, height: 150, alignment: .center)
            .cornerRadius(10)
            .background(Color(red: 0.961, green: 0.957, blue: 0.949))
            
            
            
            
        }
        Button("Start") {
            Color(.black)
            print("button tapped")
        }
    }
}

PlaygroundPage.current.setLiveView(ContentView())
