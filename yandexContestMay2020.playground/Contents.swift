import UIKit
import Foundation

//first exercise
//print(getCompare())
func getCompare() -> Bool {
    guard let str = readLine() else { return false }
    
    let ar = str.split(separator: " ")
    guard ar.count >= 2 else { return false }
    
    var firstStrArray: [String] = []
    for char in ar[0] {
        if char == "." {
            if firstStrArray.count > 0 {
                firstStrArray.removeLast()
            }
        } else {
            firstStrArray.append(String(char))
        }
    }
    
    var secondStrArray: [String] = []
    for char in ar[1] {
        if char == "." {
            if secondStrArray.count > 0 {
                secondStrArray.removeLast()
            }
        } else {
            secondStrArray.append(String(char))
        }
    }

    return firstStrArray.joined() == secondStrArray.joined()
}

// second exercise
enum OperatorType {
    case plus, minus, multi, divide, power
    
    var name: String {
        switch self {
        case .plus: return "+"
        case .minus: return "-"
        case .multi: return "*"
        case .divide: return "/"
        case .power: return "^"
        }
    }
    
    var associativityIsLeft: Bool {
        return true
    }
    
    var precedence: Int {
        switch self {
        case .plus, .minus: return 10
        case .multi, .divide: return 20
        case .power: return 30
        }
    }
    
    func result(lhs: Double, rhs: Double) -> Double {
        switch self {
        case .plus: return lhs + rhs
        case .minus: return lhs - rhs
        case .multi: return lhs * rhs
        case .divide: return lhs / rhs
        case .power: return pow(lhs, rhs)
        }
    }
    
}

func evalArithmeticExpression(_ input: String) -> Double {
    let operators: [OperatorType] = [.plus, .minus, .multi, .divide, .power]
    
    var output: [Double] = []
    var opStack: [OperatorType] = []

    for token in input.components(separatedBy: .whitespaces) {
        let number = Double(token)
        if (number != nil) {
            output += [number!]
        } else {
            let op = operators[operators.firstIndex(where: { $0.name == token })!]

            while !opStack.isEmpty {
                let topOperator = opStack.last!
                
                if op.associativityIsLeft && op.precedence <= topOperator.precedence || !op.associativityIsLeft && op.precedence < topOperator.precedence {
                    opStack.popLast()
                    let rhs = output.popLast()!, lhs = output.popLast()!
                    let result = topOperator.result(lhs: lhs, rhs: rhs)
                    
                    output.append(result)
            
                } else {
                    break
                }
            }

            opStack += [op]
        }
    }
    
    while !opStack.isEmpty {
        let topOperator = opStack.popLast()!
        
        let rhs = output.popLast()!, lhs = output.popLast()!
        let result = topOperator.result(lhs: lhs, rhs: rhs)
        
        output.append(result)
    }
    return output[0]
}

evalArithmeticExpression("1 + 2 * 3")
